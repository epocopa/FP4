/*
    This file is part of WolfWolfRun.

    WolfWolfRun is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WolfWolfRun is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WolfWolfRun.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "listacadenas.h"
#include <algorithm>

typedef struct {
  std::string clave;
  tListaCadenas valor;
} tElementoIndicePalabras;

const int MAX_ELEM = 2;//Establece numero inicial de elementos en IndicePalabras

typedef struct {
  tElementoIndicePalabras *datos;
  int tam;
  int dim;
} tIndicePalabras;

int getTam(const tIndicePalabras &l);
tElementoIndicePalabras getRegistro(const tIndicePalabras &l, int pos);
bool buscar(const tIndicePalabras &l, const std::string  &s);
int getPosicion(const tIndicePalabras &l, const std::string &s);
int getPosicionR(const tIndicePalabras &l, const std::string &s,
  int ini, int fin);
void insertar(tIndicePalabras &idx, const std::string &palabra,
              const std::string &nombreArchivo);
void imprimir(const tIndicePalabras & idx);
bool repetido(const tIndicePalabras &idx, const std::string str);
void inicializar(tIndicePalabras &idx);
void destruir(tIndicePalabras &idx);
