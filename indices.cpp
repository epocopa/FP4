/*
    This file is part of WolfWolfRun.

    WolfWolfRun is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WolfWolfRun is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WolfWolfRun.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "indices.h"

using namespace std;

void crearTabla(tIndicePalabras &tabla, tListaCadenas &totales,
                 const string &archivoInicial, tMatriz &matriz){
  tListaCadenas visitados;
  tListaCadenas noVisitados;
  inicializar(visitados);
  inicializar(noVisitados);

  cout << "[CORRECTO]\n";
  cout << "Creando tablas... ";

  //First IN first OUT
  string ntemp;
  string archivoActual;
  insertar(noVisitados, archivoInicial);
  insertar(totales, archivoInicial);
  matriz.tam = 1;
  matriz.data[0][0] = 0;
  while (noVisitados.tam != 0) {
    archivoActual = noVisitados.data[0];
    ifstream fich(archivoActual.c_str());
    if (fich.is_open()) {
      fich >> ntemp;
      while (!fich.eof()) {
        if (ntemp.at(0) == '<') { // La cadena es un enlace
          limpiarPalabra(ntemp);
          if (!buscar(totales, ntemp)) {
            for (int i = 0; i < matriz.tam; i++) {
              matriz.data[matriz.tam][i] = 0;
              matriz.data[i][matriz.tam] = 0;
            }
            matriz.data[matriz.tam][matriz.tam] = 0;
            matriz.data[matriz.tam][getPosicion(totales, archivoActual)] = 1;
            matriz.tam++;
            insertar(noVisitados, ntemp);
            insertar(totales, ntemp);
          } else {
            matriz.data[getPosicion(totales, ntemp)]
            [getPosicion(totales, archivoActual)] = 1;
          }
        } else { // La cadena es una palabra
          limpiarPalabra(ntemp);
          insertar(tabla, ntemp, archivoActual);
        }
        fich >> ntemp;
      }
    insertar(visitados, archivoActual);
		}
	eliminar(noVisitados, 0); // Si el archivo no se pudo abrir, se eliminara
  }
  destruir(visitados);
  destruir(noVisitados);
}

void limpiarPalabra(string &str) {
  while (ispunct(str.at(str.length() - 1))) {
    str.erase(str.length() - 1);
  }
  while (ispunct(str.at(0))) {
    str.erase(str.begin());
  }
}

void buscar(const tIndicePalabras &tabla, const tListaCadenas &totales,
            const tVector &rank){
  tElementoBusqueda *apar; //Apuntara a una lista de las apariciones
  string str;
  int tam, pos;

  do {
	  cout << "\n\nIntroduzca una palabra (\"fin\" para terminar): ";
	  cin >> str;
	  if (buscar(tabla, str) && str != "fin") {
      pos = getPosicion(tabla, str);
      tam = getTam(tabla.datos[pos].valor);
      apar = new tElementoBusqueda[tam];
      for (int i = 0; i < tam; i++) {
        apar[i].str = getCadena(tabla.datos[pos].valor, i);
        apar[i].rel = rank.data[getPosicion(totales, apar[i].str)];
        for (int j = tam - 1; j > 0; j--) {
          if (apar[j].rel > apar[j - 1].rel) {
            tElementoBusqueda aux = apar[j - 1];
            apar[j - 1] = apar[j];
            apar[j] = aux;
          }
        }
      }
      for (int i = 0; i < tam; i++) {
			  cout << "\n  Se ha encontrado en " << apar[i].str << " (relevancia "
         << fixed << setprecision(2) << apar[i].rel/apar[0].rel << ")\n";
		  }

      delete [] apar;
	  } else if(str != "fin") {
		 cout << "\n  La palabra introducida no se encuentra en el indice.\n";
	  }
  } while (str != "fin");
}

bool operator<(const tElementoBusqueda &a, const tElementoBusqueda &b){
  return a.rel < b.rel;
}
