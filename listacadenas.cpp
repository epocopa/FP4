/*
    This file is part of WolfWolfRun.

    WolfWolfRun is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WolfWolfRun is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WolfWolfRun.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "listacadenas.h"

using namespace std;

void redimensionar(tListaCadenas & l);


int getTam(const tListaCadenas &l){
  return l.tam;
}

string  getCadena(const  tListaCadenas  &l,  int pos){
  return l.data[pos];
}

void insertar(tListaCadenas &l, const string &s){
  if (l.tam == l.dim) {
    redimensionar(l);
  }
  l.data[l.tam] = s;
  l.tam++;
}

bool buscar(const tListaCadenas &l, const string &s){
  bool encontrado = false;
  if (getPosicion(l, s) != -1) {
    encontrado = true;
  }
  return encontrado;
}

int getPosicion(const tListaCadenas &l, const string &s){
  int pos;
  bool encontrado = false;
  int i = 0;
  while (i < l.tam && !encontrado) {
    if (l.data[i] == s) {
      pos = i;
      encontrado = true;
    }
  i++;
  }
  if (!encontrado) {
    pos = -1;
  }
  return pos;
}

void eliminar(tListaCadenas &l, int pos){
  l.tam--;
  for (int i = pos; i < l.tam; i++){
    l.data[i] = l.data[i+1];
  }
}

void imprimir(const tListaCadenas &l){
  for (int i = 0; i < l.tam; i++) {
    cout << " " << l.data[i];
  }
   cout << "\n";
}

void inicializar(tListaCadenas & l){
  l.dim = MAX_STRINGS;
  l.tam = 0;
  l.data = new string[MAX_STRINGS];
}

void destruir(tListaCadenas & l){
  delete [] l.data;
}

void redimensionar(tListaCadenas & l){
  l.dim = (l.dim * 3) / 2 + 1;
  string *lista = new string[l.dim];
  for (int i = 0; i < l.tam; i++){
    lista[i] = l.data[i];
  }
  delete [] l.data;
  l.data = lista;
}

void mover(tListaCadenas & l1, tListaCadenas & l2){
  l1.data = new string[l2.dim];
  l1.dim = l2.dim;
  l1.tam = l2.tam;
  for (int i = 0; i < l2.tam; i++){
    l1.data[i] = l2.data[i];
  }

  delete[] l2.data;
}
