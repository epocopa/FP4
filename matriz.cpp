/*
    This file is part of WolfWolfRun.

    WolfWolfRun is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WolfWolfRun is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WolfWolfRun.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "matriz.h"

tMatriz matrizConstante(double x, int n){
  tMatriz matTemp;
  matTemp.tam = n;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      matTemp.data[i][j] = x;
    }
  }
  return matTemp;
}

tVector vectorConstante(double x, int n){
	tVector vecTemp;
	vecTemp.tam = n;
	for (int i = 0; i < n; i++) {
		vecTemp.data[i] = x;
	}
	return vecTemp;
}

tVector operator*(const tMatriz & m, const tVector & v){
  tVector vecTemp;
  vecTemp.tam = v.tam;
  for (int i = 0; i < v.tam; i++) {
    vecTemp.data[i] = 0;
    for (int j = 0; j < v.tam; j++) {
      vecTemp.data[i] += m.data[i][j] * v.data[j];
    }
  }
  return vecTemp;
}

tMatriz operator*(double x, const tMatriz & m){
  tMatriz matTemp;
  matTemp.tam = m.tam;
  for (int i = 0; i < m.tam; i++) {
    for (int j = 0; j < m.tam; j++) {
      matTemp.data[i][j] = m.data[i][j] * x;
    }
  }
  return matTemp;
}

tMatriz operator+(const tMatriz & m1, const tMatriz & m2){
  tMatriz matTemp;
  matTemp.tam = m1.tam;
  for (int i = 0; i < m1.tam; i++) {
    for (int j = 0; j < m1.tam; j++) {
      matTemp.data[i][j] = m1.data[i][j] + m2.data[i][j];
    }
  }
  return matTemp;
}

tVector operator-(const tVector & v1, const tVector & v2){
	tVector vecTemp;
	vecTemp.tam = v1.tam;
	for (int i = 0; i < v1.tam; i++) {
		vecTemp.data[i] = v1.data[i] - v2.data[i];
	}
	return vecTemp;
}

tMatriz desdeEnlacesAMatriz(const tMatriz & L){
  tMatriz matM, matC;
  double N;
  matM.tam = matC.tam = L.tam;
  for (int j = 0; j < L.tam; j++) { // Calculamos M'
    N = 0;
    for (int i = 0; i < L.tam; i++) {
      N += L.data[i][j];
    }
    if (N == 0) {
      N = L.tam;
      for (int i = 0; i < L.tam; i++) {
        matM.data[i][j] = 1 / N;
      }
    } else {
      for (int i = 0; i < L.tam; i++) {
        matM.data[i][j] = L.data[i][j] / N;
      }
    }
  }
  matC = matrizConstante(1 / L.tam, L.tam); // Calculamos C
  return (1 - c) * matC + c * matM; // M = (1 - c) * Cn + c * M'
}

tVector normaliza(const tVector & v){
  tVector vecTemp;
  vecTemp.tam = v.tam;
  for (int i = 0; i < v.tam; i++) {
    vecTemp.data[i] = v.data[i] / v.data[0];
  }
  return vecTemp;
}

tVector vectorPropio(const tMatriz & M){
	bool cercano = false;
	const double dif = 0.00001;
	tVector w, w1;
	w = vectorConstante(1, M.tam);
	while (!cercano) {
		w1 = M*w;
		w = w - w1;
		cercano = true;
		for (int i = 0; i < w.tam && cercano; i++) {
			if (w.data[i] > dif) {
				cercano = false;
			}
		}
		w = w1;
	}
	return w;
}

tVector desdeMatrizAVector(const tMatriz & L){
  return normaliza(vectorPropio(desdeEnlacesAMatriz(L)));
}
