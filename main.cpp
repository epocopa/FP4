/*
    This file is part of WolfWolfRun.

    WolfWolfRun is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WolfWolfRun is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WolfWolfRun.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "indices.h"
using namespace std;

string nombrefichero(){
  string nombre;
  bool existe = false;
  do {
    cout << "Por favor, introduzca el nombre del fichero raiz\n" <<
      "a partir del que se creara el indice: ";
    cin >> nombre;
    ifstream fich(nombre.c_str());
    if (fich.is_open()) {
      existe = true;
      fich.close();
    } else{
      cout << "\n El fichero no se ha podido abrir\n\n";
    }
  } while(!existe);
  return nombre;
}

int main(){
  tIndicePalabras indice;
  tListaCadenas totales;
  tMatriz matriz;
  inicializar(indice);
  inicializar(totales);

  tVector rank;
  string nombre;

  cout << "   WolfWolfRun\n";
  cout << "------------------------\n\n";
  nombre = nombrefichero();
  cout << "Cargando... ";
  crearTabla(indice, totales, nombre, matriz);
  cout << "[CORRECTO]\n";
  imprimir(indice);

  rank = desdeMatrizAVector(matriz);
  buscar(indice, totales, rank);

  destruir(indice);
  destruir(totales);

  return 0;
}
