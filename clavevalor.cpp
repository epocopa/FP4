/*
    This file is part of WolfWolfRun.

    WolfWolfRun is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WolfWolfRun is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WolfWolfRun.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "clavevalor.h"

using namespace std;

void redimensionar(tIndicePalabras &idx);

int getTam(const tIndicePalabras &l){
  return l.tam;
}

tElementoIndicePalabras getRegistro(const tIndicePalabras &l, int pos){
  return l.datos[pos];
}

bool buscar(const tIndicePalabras &l, const string &s){
    bool encontrado;
    int pos = getPosicionR(l, s, 0, l.tam - 1);
    if (pos < l.tam && s == l.datos[pos].clave) {
      encontrado = true;
    } else {
      encontrado = false;
    }
    return encontrado;
}

int getPosicion(const tIndicePalabras &l, const string &s){
  int pos = 0;
  int i = 0;
  while (i < l.tam && s > l.datos[i].clave) {
    i++;
    pos = i;
  }
  return pos;
}

int getPosicionR(const tIndicePalabras &l, const string &s,
                int ini, int fin){
  int pos, mitad = (ini + fin) / 2;
  if (ini <= fin) {
    if (s == l.datos[mitad].clave) {
      pos = mitad;
    }
      else if (s < l.datos[mitad].clave) {
      pos = getPosicionR(l, s, ini, mitad - 1);
    }
    else{
      pos = getPosicionR(l, s, mitad + 1, fin);
    }
   } else{
     pos = ini;
   }
   return pos;
}

void insertar(tIndicePalabras &idx, const string &palabra,
              const string &nombreArchivo){
  string str = palabra;
  std::transform(str.begin(), str.end(), str.begin(), ::tolower);
  bool existe = buscar(idx, str);
  if(!existe){
    int pos = getPosicion(idx, str);
    if (idx.dim == idx.tam) {
      redimensionar(idx);
    }
    for (int i = idx.tam; i > pos; i--) {
      idx.datos[i].clave = idx.datos[i-1].clave;
      mover(idx.datos[i].valor, idx.datos[i - 1].valor);
    }
    inicializar(idx.datos[pos].valor);
    idx.datos[pos].clave = str;
    insertar(idx.datos[pos].valor, nombreArchivo);
    idx.tam++;
  } else{
    int pos = getPosicion(idx, str);
    if(idx.datos[pos].valor.data[idx.datos[pos].valor.tam-1]
            != nombreArchivo) {
      insertar(idx.datos[pos].valor, nombreArchivo);
    }
  }
}


void imprimir(const tIndicePalabras &idx){
  int a;
  for (int i = 0; i < idx.tam; i++) {
    a = idx.datos[i].valor.tam;
    cout << left << setw(2) << i << " " << setw(18)
    << idx.datos[i].clave <<  "  ";
    imprimir(idx.datos[i].valor);
  }
}

void inicializar(tIndicePalabras &idx){
  idx.dim = MAX_ELEM;
  idx.tam = 0;
  idx.datos = new tElementoIndicePalabras[MAX_ELEM];
}
void destruir(tIndicePalabras &idx){
  for (int i = 0; i < idx.tam; i++) {
    destruir(idx.datos[i].valor);
  }
  delete [] idx.datos;
}
void redimensionar(tIndicePalabras &idx){
  idx.dim = (idx.dim * 3) / 2 + 1;
  tElementoIndicePalabras *indice = new tElementoIndicePalabras[idx.dim];
  for (int i = 0; i < idx.tam; i++){
    indice[i] = idx.datos[i];
  }
  delete[] idx.datos;
  idx.datos = indice;
}
