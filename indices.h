/*
    This file is part of WolfWolfRun.

    WolfWolfRun is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WolfWolfRun is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WolfWolfRun.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "clavevalor.h"
#include "matriz.h"
#include <fstream>

typedef struct {
  std::string str;
  double rel;
} tElementoBusqueda;

void crearTabla(tIndicePalabras &tabla, tListaCadenas &totales,
              const std::string &archivoInicial, tMatriz &l);
void limpiarPalabra(std::string &str);
void buscar(const tIndicePalabras &tabla, const tListaCadenas &totales,
            const tVector &rank);
bool operator<(const tElementoBusqueda &a, const tElementoBusqueda &b);
