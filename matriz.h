/*
    This file is part of WolfWolfRun.

    WolfWolfRun is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WolfWolfRun is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WolfWolfRun.  If not, see <http://www.gnu.org/licenses/>.
*/


const int MAX_TAM = 10;//Establece numero maximo de elementos en matriz y vector
const double c = 0.85;

typedef struct {
  int tam;
  double data[MAX_TAM][MAX_TAM];
} tMatriz;

typedef struct {
  int tam;
  double data[MAX_TAM];
} tVector;

tMatriz matrizConstante(double x, int n);
tVector operator*(const tMatriz & m, const tVector & v);
tMatriz operator*(double x, const tMatriz & m);
tMatriz operator+(const tMatriz & m1, const tMatriz & m2);
tMatriz desdeEnlacesAMatriz(const tMatriz & L);
tVector normaliza(const tVector & v);
tVector vectorPropio(const tMatriz & M);
tVector desdeMatrizAVector(const tMatriz & L);
