/*
    This file is part of WolfWolfRun.

    WolfWolfRun is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WolfWolfRun is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WolfWolfRun.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>
#include <iostream>
#include <iomanip>

const int MAX_STRINGS = 2;//Establece numero inicial elementos en ListaCadenas

typedef struct {
  int tam;
  int dim;
  std::string *data;
} tListaCadenas;

int getTam(const tListaCadenas & l);
std::string getCadena(const tListaCadenas & l, int pos);
void insertar(tListaCadenas & l, const std::string & s);
bool buscar(const tListaCadenas & l, const std::string & s);
int getPosicion(const tListaCadenas & l, const std::string & s);
void eliminar(tListaCadenas & l, int pos);
void imprimir(const tListaCadenas & l);
void inicializar(tListaCadenas & l);
void destruir(tListaCadenas & l);
void mover(tListaCadenas & l1, tListaCadenas & l2);
